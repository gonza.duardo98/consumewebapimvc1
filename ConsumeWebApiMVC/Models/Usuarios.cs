﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsumeWebApiMVC.Models
{
    public class Usuarios
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string loginName { get; set; }
        public int profileId { get; set; }
    }
}
