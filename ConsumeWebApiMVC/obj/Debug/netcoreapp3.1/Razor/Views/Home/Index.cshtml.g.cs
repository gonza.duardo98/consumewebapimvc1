#pragma checksum "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "768a219adf116322d665d2d0f027704815433cb3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\_ViewImports.cshtml"
using ConsumeWebApiMVC;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\_ViewImports.cshtml"
using ConsumeWebApiMVC.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"768a219adf116322d665d2d0f027704815433cb3", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"94d03b1819ad43eb2c7c7c2f843a2af93152bb52", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<ConsumeWebApiMVC.Models.Usuarios>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
  
    ViewBag.Title = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h2>Consumiendo Web Api</h2>\r\n\r\n<p>");
#nullable restore
#line 9 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
Write(Html.ActionLink("Nuevo", "Create"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n\r\n<table class=\"table\">\r\n    <th>id</th>\r\n    <th>firstName</th>\r\n    <th>lastName</th>\r\n    <th>loginName</th>\r\n    <th>profileId</th>\r\n    <th></th>\r\n\r\n");
#nullable restore
#line 19 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
     foreach (var item in Model)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
#nullable restore
#line 23 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.id));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 26 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.firstName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 29 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.lastName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 32 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.loginName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 35 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.profileId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 38 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
           Write(Html.ActionLink("Edita", "Edit", new { /* id=item.PrimaryKey */}));

#line default
#line hidden
#nullable disable
            WriteLiteral(" |\r\n                ");
#nullable restore
#line 39 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"
           Write(Html.ActionLink("Elimina", "Delete", new { /* id=item.PrimaryKey */}));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 42 "C:\Users\Marsh\source\repos\ConsumeWebApiMVC\ConsumeWebApiMVC\Views\Home\Index.cshtml"

    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<ConsumeWebApiMVC.Models.Usuarios>> Html { get; private set; }
    }
}
#pragma warning restore 1591
