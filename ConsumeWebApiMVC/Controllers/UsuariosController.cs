﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsumeWebApiMVC.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
//using System.Web.Mvc;

namespace ConsumeWebApiMVC.Controllers
{
    public class UsuariosController : Controller
    {
        //URL
        string Baseurl = "api.losapis.programacionxyz.com";

        public async Task<ActionResult> Index()
        {
            List<Usuarios> EmpInfo = new List<Usuarios>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("api/user");
                if(Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    EmpInfo = JsonConvert.DeserializeObject<List<Usuarios>>(EmpResponse);
                }
                return View(EmpInfo);
            }
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
